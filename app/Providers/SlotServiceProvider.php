<?php

namespace App\Providers;

class SlotServiceProvider
{
    protected $symbols = ['9', '10', 'J', 'Q', 'K', 'A'];
    protected $payLines = [
        [0, 3, 6, 9, 12],
        [1, 4, 7, 10, 13],
        [2, 5, 8, 11, 14],
        [0, 4, 8, 10, 12],
        [2, 4, 6, 10, 14]
    ];

    protected $numbering = [
        [0, 3, 6, 9, 12],
        [1, 4, 7, 10, 13],
        [2, 5, 8, 11, 14]
    ];

    protected $board = [];
    protected $simpleBoard = [];
    protected $betAmount = 100;
    protected $totalWin = 0;

    public function __construct()
    {
        $this->generateBoard();
        $this->checkPayLines();

    }

    private function generateRandomSymbol()
    {
        return $this->symbols[rand(0, count($this->symbols) - 1)];
    }


    /*
       0 3 6 9 12
       1 4 7 10 13
       2 5 8 11 14
     */
    private function getNumbering($row, $col)
    {
        return $this->numbering[$row][$col];
    }

    public function calculateWinnings(array $winnings)
    {
        $sum = 0;
        foreach ($winnings as $encounter) {
            $sum += ($this->betAmount * $encounter);
        }

        return $sum;
    }

    public function generateBoard()
    {
        $this->board = [];
        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 5; $j++) {
                $this->board[$i][$this->numbering[$i][$j]] = $this->generateRandomSymbol();
            }
            $this->simpleBoard = array_replace($this->simpleBoard, $this->board[$i]);
        }

        ksort($this->simpleBoard);
    }

    public function checkPayLines()
    {
        foreach ($this->board as $row => $rowValue) {
            foreach ($this->board[$row] as $col => $colValue) {
                echo str_pad($colValue, 7);
            }
            echo "\n";
        }

        $out = [];

        $lastSymbol = null;
        foreach ($this->payLines as $k => $line) {
            $encounters = 0;
            foreach ($line as $number) {
                if ($lastSymbol === null) {
                    $lastSymbol = $this->simpleBoard[$number];
                }
                if ($this->simpleBoard[$number] == $lastSymbol) {
                    $encounters++;
                } else {
                    if ($encounters > 2) {
                        $out[$k] = $encounters;
                    }
                    break;
                }

                $lastSymbol = $this->simpleBoard[$number];
            }
        }

        if (count($out)) {
            //print_r($this->simpleBoard);
            //print_r($out);
            echo "####################\n";
        }

        return $out;
    }
}
