<?php

namespace App\Slots;

class Slots
{
    protected $symbols = ['9', '10', 'J', 'Q'];
    protected $payLines = [
        [0, 3, 6, 9, 12],
        [1, 4, 7, 10, 13],
        [2, 5, 8, 11, 14],
        [0, 4, 8, 10, 12],
        [2, 4, 6, 10, 14]
    ];

    protected $numbering = [
        [0, 3, 6, 9, 12],
        [1, 4, 7, 10, 13],
        [2, 5, 8, 11, 14]
    ];

    private $results = [];

    protected $board = [];
    protected $simpleBoard = [];
    protected $betAmount = 100;
    protected $totalWin = 0;

    private function generateRandomSymbol()
    {
        return $this->symbols[rand(0, count($this->symbols) - 1)];
    }


    /*
       0 3 6 9 12
       1 4 7 10 13
       2 5 8 11 14
     */
    private function getNumbering($row, $col)
    {
        return $this->numbering[$row][$col];
    }

    public function getBetAmount(): int
    {
        return $this->betAmount;
    }

    public function calculateWinnings()
    {
        $sum = 0;
        foreach ($this->results as $encounter) {
            if ($encounter == 3) {
                $sum += $this->betAmount * 1.2;
            } elseif ($encounter == 4) {
                $sum += $this->betAmount * 2;
            } else {
                $sum += $this->betAmount * 10;
            }
        }

        $this->totalWin = $sum;
    }

    public function spin()
    {
        $this->generateBoard();
        $this->checkPayLines();
        $this->calculateWinnings();
    }

    public function generateBoard()
    {
        $this->board = [];
        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < 5; $j++) {
                $this->board[$i][$this->numbering[$i][$j]] = $this->generateRandomSymbol();
            }
            $this->simpleBoard = array_replace($this->simpleBoard, $this->board[$i]);
        }

        ksort($this->simpleBoard);
    }

    public function checkPayLines()
    {
        $out = [];

        $lastSymbol = null;
        foreach ($this->payLines as $k => $line) {
            $encounters = 0;
            foreach ($line as $index => $number) {
                if ($index == 0) {
                    $lastSymbol = $this->simpleBoard[$number];
                }
                if ($this->simpleBoard[$number] == $lastSymbol) {
                    $encounters++;
                } else {
                    if ($encounters > 2) {
                        $out[$k] = $encounters;
                    }

                    break;
                }

                $lastSymbol = $this->simpleBoard[$number];
            }
        }

        $this->results = $out;
    }

    public function getWinningPayLines(): array
    {
        $out = [];
        if ($this->results) {
            foreach ($this->results as $k => $v) {
                $out[] = join(',', $this->payLines[$k]);
            }
        }

        return $out;
    }

    public function getSimpleBoardValues()
    {
        return array_values($this->simpleBoard);
    }

    public function getBoard()
    {
        return $this->board;
    }

    public function getTotalWin(): int
    {
        return $this->totalWin;
    }

    public function isWinner()
    {
        return $this->totalWin ? false : true;
    }
}
