<?php

namespace App\Console\Commands;

use App\Slots\Slots;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class SpinCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'slots:spin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Spin the slot machine";

    protected $signature = 'slot:spin {draw=no}';

    protected $slot;


    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
    }

    public function handle()
    {
        $slot = new Slots();
        $slot->spin();

        $board = $slot->getBoard();

        if($this->argument('draw') === "yes") {
            $this->output->writeln('-----------------------------------------');
            foreach ($board as $row => $rowValue) {
                foreach ($board[$row] as $col => $colValue) {
                    $this->output->write('|' . str_pad($colValue, 7));
                }
                echo "|\n";
            }
            $this->output->writeln('-----------------------------------------');
        }

        $this->output->writeln('board: ' . join(',', $slot->getSimpleBoardValues()));
        $this->output->writeln('paylines: ' . join("\n", $slot->getWinningPayLines()));
        $this->output->writeln('bet_amount: ' . $slot->getBetAmount());
        $this->output->writeln('total_win: ' . $slot->getTotalWin());
    }

}
